terraform {
  required_version = ">= 0.13"

  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

provider "kubernetes" {
  config_path = "~/.kube/k3s"
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/k3s"
  }
}

provider "kubectl" {
  config_path = "~/.kube/k3s"
}

resource "kubernetes_namespace" "example" {
  metadata {
    name = "my-first-namespace"
  }
}

resource "kubernetes_deployment" "vscode" {
  metadata {
    name = "vscode"
    labels = {
      component = "vscode"
    }
  }
  spec {
    replicas = 1
    selector {
      match_labels = {
        component = "vscode"
      }
    }

    template {
      metadata {
        labels = {
          component = "vscode"
        }
      }
      spec {
        container {
          image = "registry.gitlab.com/nicolalandro/vscode_docker"
          name  = "vscode"
          port {
            container_port = 8080
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "vscode" {
  metadata {
    name = "vscode"
  }
  spec {
    selector = {
      component = "vscode"
    }
    port {
      node_port   = 30201
      port        = 8080
      target_port = 8080
    }

    type = "NodePort"
  }
}

resource "helm_release" "kedacore" {
  name = "keda"

  repository = "https://kedacore.github.io/charts"
  chart      = "keda"
}

resource "helm_release" "http-keda-addon" {
  name = "keda-http"

  repository = "https://kedacore.github.io/charts"
  chart      = "keda-add-ons-http"
}

resource "kubectl_manifest" "keda_scaler" {
  yaml_body = <<YAML
kind: HTTPScaledObject
apiVersion: http.keda.sh/v1alpha1
metadata:
    name: podinfo
spec:
    host: "localhost"
    targetPendingRequests: 1
    scaleTargetRef:
        deployment: vscode
        service: vscode
        port: 8080
    replicas:
        min: 1
        max: 2
YAML
}
