# Keda for scaling http server
This is a study for keda and http server.

## Roadmap
* [x] documentation
* [x] base terraform script
* [x] run vscode service
* [x] setup kubernates service
* [ ] keda

# How to run
* with k3s installed and config into path "~/.kube/k3s" you can run the terrafomr file, to create infrastructure
```
cd terraform
terraform init
terraform plan
terraform apply # if prehempion problems deploy incrementally
```
To destroy infrastructure:
```
terraform destroy
```

## References
* docekr
* kubernates, k3s
* terraform